# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelSQL
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Id
from trytond.tools.multivalue import migrate_property
from trytond.modules.company.model import CompanyValueMixin


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    sale_sequence = fields.MultiValue(
        fields.Many2One('ir.sequence', 'Sale Sequence',
            domain=[
                ('company', 'in', [Eval(
                    'context', {}).get('company', -1), None]),
                ('sequence_type', '=', Id('sale', 'sequence_type_sale'))
            ])
    )
    sale_sequences = fields.One2Many('party.party.sale_sequence', 'party',
        'Sale Sequences')

    @classmethod
    def default_sale_sequence(cls, **pattern):
        return cls.multivalue_model('sale_sequence').default_sale_sequence()


class PartySaleSequence(ModelSQL, CompanyValueMixin):
    "Party Sale Sequence"
    __name__ = 'party.party.sale_sequence'

    party = fields.Many2One('party.party', 'Party', select=True,
        ondelete='CASCADE')
    sale_sequence = fields.Many2One('ir.sequence', 'Sale Sequence',
            domain=[
                ('company', 'in', [Eval(
                    'context', {}).get('company', -1), None]),
                ('sequence_type', '=', Id('sale', 'sequence_type_sale'))
            ])

    @classmethod
    def __register__(cls, module):
        table_h = cls.__table_handler__(module)
        exist = table_h.table_exist(cls._table)

        super(PartySaleSequence, cls).__register__(module)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('sale_sequence')
        value_names.append('sale_sequence')
        fields.append('company')
        migrate_property('party.party', field_names, cls, value_names,
            parent='party', fields=fields)

    @classmethod
    def default_sale_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('sale', 'sequence_sale')
        except KeyError:
            return None


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    @classmethod
    def set_number(cls, sales):
        custom_sales, std_sales = [], []
        for sale in sales:
            if sale.number:
                continue
            if not sale.party.sale_sequence:
                std_sales.append(sale)
                continue
            sale.number = sale.party.get_multivalue(
                'sale_sequence',
                company=sale.company.id).get()
            custom_sales.append(sale)
        if custom_sales:
            cls.save(custom_sales)
        if std_sales:
            super(Sale, cls).set_number(std_sales)
